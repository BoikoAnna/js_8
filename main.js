//Теоретичні питання
//Опишіть своїми словами що таке Document Object Model (DOM)
//сторінка у вигляді об'єктів, які можна міняти, переміщати, видаляти
//все, що записано в HTML, є в DOM-дереві, включаючи коментарі,
//це те за допомогою чого Javascript отримує оступ до контенту веб сторінки

//Яка різниця між властивостями HTML-елементів innerHTML та innerText?
//innerHTML - повертає всі дочірні елементи вкладені всередині тега (тег HTML + текстовий вміст)
//innerText - повертає тестовий зміст дочірніх елементів вкладених всередині
//різниця між ними у тому, що innerHTML проаналізує отриманий контент та інтерпретує теги, а 
//innerText буде читати все як текст включаючи строку тегів HTML

//Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
//існує глобальна змінна з назвою id, яка посилається на елемент 
//document. getElementById() та document.querySelector('#');
//краще використовувати останній варіант - найсучасніший 

//Завдання
//Код для завдань лежить в папці project.
//Знайти всі параграфи на сторінці та встановити колір фону #ff0000
//Знайти елемент із id="optionsList". Вивести у консоль.
//Знайти батьківський елемент та вивести в консоль. 
//Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
//Встановіть в якості контента елемента з класом testParagraph 
//наступний параграф -This is a paragraph
//Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль.
// Кожному з елементів присвоїти новий клас nav-item.
//Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.


const itemsList =document.querySelectorAll('p');
itemsList.forEach( item => {
item.style.background = "#ff0000";
});
console.log(itemsList);


const opt =document.getElementById('optionsList');
console.log(opt);
console.log(opt.parentElement);

if (opt.hasChildNodes()) {
console.log(opt.childNodes);
opt.childNodes.forEach(item =>{
console.log(item.nodeName);
console.log(item.nodeType);
})
}

const testP = document.querySelector('#testParagraph');
testP.textContent = "This is a paragraph";
console.log(testP);

const mainHead = document.querySelector('.main-header');
console.log(mainHead);
for (let item of mainHead.children) {
item.classList.add ('nav-item');
}
console.log(mainHead.children);


const remove =document.querySelectorAll('.section-title');
remove.forEach (item => item.classList.remove('section-title'));
console.log (remove);